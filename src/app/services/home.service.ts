import { HttpClient, HttpHeaders } from '@angular/common/http';//importamos estos dos modulos para hacer peticiones a la api
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';//este modulo sirve para interceptar datos token o passport
import { EnvService } from './env.service';//importamos este servicio que creamos anteriormente donde esta nuestra URL de la Apirest
import { Lugares } from '../models/Lugares';
import { Actividades } from '../models/Actividades';
import { ActivatedRoute } from '@angular/router';

import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private http: HttpClient,private env: EnvService,private activateRoute:ActivatedRoute) { }

  getlugares()
  {
  
    return this.http.get<Lugares>(this.env.API_URL + 'lugares')
    .pipe(
      tap(lugares => {
        return lugares;
      })
    );
  }

  getactividades(id:String){
  
    //this.id=this.activateRoute.snapshot.paramMap.get('id');
    return this.http.get<Actividades>(this.env.API_URL + 'actividades?id=' + id).pipe(
      tap( actividades => {
        return actividades;
        })
      );
  }
}
