import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetallelugarPage } from './detallelugar.page';

const routes: Routes = [
  {
    path: '',
    component: DetallelugarPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetallelugarPageRoutingModule {}
