import { Component, OnInit } from '@angular/core';
import { HomeService } from 'src/app/services/home.service';
import { Actividades } from 'src/app/models/Actividades';
import { Lugares } from 'src/app/models/Lugares';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detallelugar',
  templateUrl: './detallelugar.page.html',
  styleUrls: ['./detallelugar.page.scss'],
})
export class DetallelugarPage implements OnInit {

  actividades: Actividades;
  lugares:Lugares;

  constructor(public homeService: HomeService,private router:Router,private activatedRouted:ActivatedRoute) { 
    this.activatedRouted.params.subscribe( params => {
      //console.log(params);
      this.getactividad(params['id']);
    });
  }

  getactividad(id:string)
  {
    this.homeService.getactividades(id).subscribe(
      actividades => {
        this.actividades = actividades;
      });

  }

  ngOnInit() {

  }


}
