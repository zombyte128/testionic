import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetallelugarPageRoutingModule } from './detallelugar-routing.module';

import { DetallelugarPage } from './detallelugar.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetallelugarPageRoutingModule
  ],
  declarations: [DetallelugarPage]
})
export class DetallelugarPageModule {}
