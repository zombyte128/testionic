import { Component } from '@angular/core';
import { HomeService } from 'src/app/services/home.service';
import { Lugares } from 'src/app/models/Lugares';
import { Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  lugares:Lugares;

  constructor(public homeService: HomeService,private router:Router) {}

  verLugar( lugar:Lugares){
    
    this.router.navigate(['/detallelugar',lugar.id]);
  }
  ngOnInit() { 
    this.homeService.getlugares().subscribe(
    lugares => {
      this.lugares = lugares;
    });
  }

}
